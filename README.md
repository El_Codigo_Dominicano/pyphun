## Pyphun
Various python programs some with unit-testing some with enforced specifications using assert statements; file handling, functional programming, games, sorting, organizing, automation, image handling, etc

## Description
A modest developers portfolio, to display and furthur increase my knowledge within the 'Object-Oriented' programming language, python.

## Visuals
images - TBD

## Questions - Support
email - TBD

## License
Open Source can be used on a wide variety of systems that allows for python!

## Project status
On going
